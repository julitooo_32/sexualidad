$(document).ready(function(){
    validation = new Date()
    validation.setFullYear(validation.getFullYear() -18)
    format= `${validation.getFullYear()}-${(validation.getMonth()+1)}-${validation.getDate()}`
    console.log(format)
    $("#formulario").validate({
        errorClass:"is-invalid",
        rules:{
            nombre:{
                required:true
            },
            correo:{
                required: true,
                email: true
            },
            fnacimiento:{
                required:true,
                max:format
            }
        },
        messages:{
            nombre:{
                required:"Debe ingresar su nombre"
            },
            correo:{
                required:"Debe ingresar su correo",
                email:"Debe ingresar un correo valido"
            },
            fnacimiento:{
                required:"Debes ingresar tu fecha de nacimiento",
                max:"Debes ser mayor de edad para continuar"
            }
        }
    })
})

$("#formulario").submit(function(){
    if($("#formulario").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<a href>Why do I have this issue?</a>'
          })
    }
    return false
})